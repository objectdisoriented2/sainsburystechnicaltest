package daykin.rob.sainsburysscreenscraper.controller;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;

import controller.web.WebController;
import product.Product;
import writer.SainsburysProductJsonWriter;

public class AppController {

	@Autowired
	WebController webController;
	
	@Autowired
	SainsburysProductJsonWriter jsonWriter;

	@Autowired 
	String url;

	public void generateJson(){
		Product[] products;
		try{
			products = webController.getProductsFromUrl(url);
			jsonWriter.displayProductsAsJson(products);
		}catch(IOException e){
			System.err.println("COULD NOT FIND REQUIRED PAGES. NO PRODUCTS RETURNED.");
		}
	}

}
