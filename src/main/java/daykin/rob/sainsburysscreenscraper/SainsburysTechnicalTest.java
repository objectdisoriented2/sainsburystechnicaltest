package daykin.rob.sainsburysscreenscraper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import config.ScreenScraperApplicationConfig;
import daykin.rob.sainsburysscreenscraper.controller.AppController;

public class SainsburysTechnicalTest {

	private static ApplicationContext context;

	public static void main(String[] args){

		context = new AnnotationConfigApplicationContext(ScreenScraperApplicationConfig.class);
		AppController controller = (AppController) context.getBean("appController");
		controller.generateJson();

	}
}
