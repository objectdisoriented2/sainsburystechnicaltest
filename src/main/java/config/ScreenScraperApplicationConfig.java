package config;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import controller.web.WebController;
import daykin.rob.sainsburysscreenscraper.controller.AppController;
import product.factory.ProductFactory;
import web.connection.PageFinder;
import web.scraper.impl.TechTestProductDetailsPageScraper;
import web.scraper.impl.TechTestProductListPageScraper;
import writer.SainsburysProductJsonWriter;

@Configuration
public class ScreenScraperApplicationConfig {
	
	@Bean
	public AppController appController(){
		return new AppController();
	}
	
	@Bean
	public TechTestProductListPageScraper listPageScraper(){
		return new TechTestProductListPageScraper();
	}
	
	@Bean
	public TechTestProductDetailsPageScraper detailsPageScraper(){
		return new TechTestProductDetailsPageScraper();
	}
	
	@Bean
	public ProductFactory productFactory(){
		return new ProductFactory();
	}
	
	@Bean
	public PageFinder pageFinder(){
		return new PageFinder();
	}
	
	@Bean
	public WebController webController(){
		return new WebController();
	}
	
	@Bean
	public SainsburysProductJsonWriter jsonWriter(){
		return new SainsburysProductJsonWriter();
	}
	
	@Bean
	public RoundingMode roundingMode(){
		RoundingMode roundingMode = RoundingMode.valueOf("HALF_UP");
		return roundingMode;
	}
	
	@Bean
	public DecimalFormat moneyFormat(){
		DecimalFormat moneyFormat = new DecimalFormat("0.00");
		moneyFormat.setRoundingMode(roundingMode());
		return moneyFormat;
	}
	
	@Bean
	public String url(){
		return "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
	}

}
