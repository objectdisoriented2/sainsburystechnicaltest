package web.scraper.impl;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import web.scraper.TechTestProductPageScraper;
import web.scraper.exception.IncorrectElementException;

public class TechTestProductDetailsPageScraper extends TechTestProductPageScraper{

	protected Element getTitleElement(Document doc) throws IncorrectElementException {
		return getSingleElementByTag(doc,"title");
	}

	protected Element getProductDescriptionElement(Document doc) throws IncorrectElementException {
		return getSingleElementByAttributeValue(doc,"name","description");
	}

	protected Element getPricePerUnitElement(Document doc) throws IncorrectElementException {
		return getSingleElementByClass(doc,"pricePerUnit");
	}

	protected Element getProductKcalPerHundredGramsElement(Document doc) throws IncorrectElementException {
		Element nutritionalTable = getSingleElementByClass(doc,"nutritionTable");
		Element nutritionLevel1 = getSingleElementByAttributeValue("class","nutritionLevel1",nutritionalTable);
		return nutritionLevel1;
	}
	
	protected Element getProductKcalPerHundredGramsSecondWayElement(Document doc) throws IncorrectElementException {
		Element nutritionalTable = getSingleElementByClass(doc,"nutritionTable");
		Elements elements = nutritionalTable.getElementsByClass("tableRow0");
		Element nutritionLevel1 = null;
		for(Element element: elements){
			if(element.text().contains("kcal")){
				nutritionLevel1 = element;
			}
		}
		return nutritionLevel1;
	}
	
	protected Element getProductKcalPerHundredGramsThirdWayElement(Document doc) throws IncorrectElementException {
		Element nutritionalTable = getSingleElementByAttributeValue("class","nutritionTable",doc);
		Elements elements = nutritionalTable.getElementsByClass("tableRow0");
		Element nutritionLevel1 = null;
		for(Element element: elements){
			if(element.getElementsByClass("")!=null && element.getElementsByClass("").get(0).text().contains("kcal")){
				nutritionLevel1 = element.getElementsByClass("").get(0);
			}
		}
		return nutritionLevel1;
	}

	public String getProductTitle(Document doc) throws IncorrectElementException {
		Element element = getTitleElement(doc);
		return element.text();
	}

	public String getProductDescription(Document doc) throws IncorrectElementException {
		Element element = getProductDescriptionElement(doc);
		return element.attr("content");
	}

	public double getProductPricePerUnit(Document doc){
		try{
			Element element = getPricePerUnitElement(doc);
			return Double.parseDouble(element.ownText().substring(1));
		}catch(IncorrectElementException e){
			return 0.0;
		}
	}
	
	public int getProductKcalPerHundredGrams(Document doc){
		int kcal = 0;
		try{
			Element element = getProductKcalPerHundredGramsElement(doc);
			kcal = Integer.parseInt(element.ownText().substring(0, element.ownText().length()-4));
			if(kcal == 0){
				element = getProductKcalPerHundredGramsSecondWayElement(doc);
				kcal = Integer.parseInt(element.ownText().substring(0, element.ownText().length()-4));
			}
			return kcal;
		}catch(IncorrectElementException e){
			try{
				Element element = getProductKcalPerHundredGramsSecondWayElement(doc);
				if(element!=null && element.ownText().length()>=5){
					kcal = Integer.parseInt(element.ownText().substring(0, element.ownText().length()-4));
				}
				return kcal;
			}catch(IncorrectElementException ex){
				try{
					Element element = getProductKcalPerHundredGramsThirdWayElement(doc);
					if(element!=null && element.ownText().length()>=5){
						kcal = Integer.parseInt(element.ownText().substring(0, element.ownText().length()-4));
					}
					return kcal;
				}catch(IncorrectElementException exe){
					return 0;
				}
			}
		}
	}
	
	

}
