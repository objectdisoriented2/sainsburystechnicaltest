package product.factory;

import product.Product;

public class ProductFactory {
	
	public Product createProduct(String name, String description, double pricePerUnit, int kcal){
		Product product = new Product();
		product.setName(name);
		product.setDescription(description);
		product.setPricePerUnit(pricePerUnit);
		product.setKcalPerHundredGrams(kcal);
		return product;
	}

}
