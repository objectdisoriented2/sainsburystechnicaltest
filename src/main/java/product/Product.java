package product;

public class Product {
	
	private String name;
	private String description;
	private double pricePerUnit;
	private int kcalPerHundredGrams;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getPricePerUnit() {
		return pricePerUnit;
	}
	
	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}
	
	public int getKcalPerHundredGrams() {
		return kcalPerHundredGrams;
	}
	
	public void setKcalPerHundredGrams(int kcal) {
		this.kcalPerHundredGrams = kcal;
	}
	
	
}
