# Sainsburys Technical Test

Please run the app by downloading the jar file sainsburysscreenscraper-0.0.1-SNAPSHOT-jar-with-dependencies.jar and running it from the command line with the command 

java -jar sainsburysscreenscraper-0.0.1-SNAPSHOT-jar-with-dependencies.jar

The unit tests can be run by downloading the source from the public bitbucket repository and running them from a suitable IDE, i.e. Eclipse.

The jar is built with dependencies built in so there should be no requirement for additional dependencies.
